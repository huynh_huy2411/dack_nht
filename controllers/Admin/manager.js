const express = require('express');
const router = express.Router();
const nodemailer = require('nodemailer');

const account = require('../../models/account.M');
const product = require('../../models/product');
const category = require('../../models/category');
const sub_cate = require('../../models/sub_categories');
const upgrade = require('../../models/bidder_product/bidder_upgrade');

const bodyParser = require('body-parser');

var jsonParser = bodyParser.json();

var urlencodedParser = bodyParser.urlencoded({ extended: true });

router.use('/admin', async (req, res, next) => {
    if (req.session.user == null) {
        res.redirect('/login');
    }
    else {
        const acc = await account.getByID(req.session.user);
        if (acc.user_Decentralized != 4) {
            res.redirect('/');
        }
        next();
    }   
})

router.get('/admin/mnproduct', async (req, res) => {
    const pro = await product.all();
    res.render('home', {
        Success: true,
        ps : pro,
        mnproduct: true,
        mncategory: false,
        mnaccount: false,
        Session: req.session.user,

    })
})

router.get('/admin/mncategory=:edit', async (req, res) => {
    const edit = req.params.edit;
    console.log(edit);
    if(edit == 1) {
         m_edit = true;
    } else {
         m_edit = false;
    }
    const cate = await category.all();
    for (let c of cate) {
       var n = await sub_cate.allByCatID(c.CatID);
       c.sub = n;
       for (let a of c.sub) {
           a.mode = m_edit;
       }
       c.mode = m_edit;
    }

    res.render('home', {
        Success: true,
        mnproduct: false,
        mncategory: true,
        mnaccount: false,
        cate: cate,
        edit: m_edit,
        Session: req.session.user,
    })
})

router.get('/admin/mnaccount', async (req, res) => {
    
    const acc = await account.getAll();
    var admin = [];
    var bidder = [];
    var seller = [];
    for (let one of acc){
        if (one.user_Decentralized == 4) {
            admin.push(one);
        }
        if (one.user_Decentralized == 3) {
            seller.push(one);
        }
        if (one.user_Decentralized == 2) {
            const kt = await upgrade.getNewUpgrade(one.user_ID);
            if (kt.length != 0) {
                one.up = true;
            } else {
                one.up = false;
            }
         
            bidder.push(one);
        }
    }
    res.render('home', {
        Success: true,
        mnproduct: false,
        mncategory: false,
        mnaccount: true,
        admin: admin,
        seller: seller,
        bidder: bidder,
        Session: req.session.user,
    })
})

router.post('/admin/:decent/:id', async (req, res) => {
    if (req.params.decent == 2) {
        const id = req.params.id;
        const rs = await upgrade.setUpgrade(id);
        const up = await account.setUpgradeBidder(id);
        res.redirect('/admin/mnaccount');
    } else {
        const id = req.params.id;
        const up = await account.setDropSeller(id);
        res.redirect('/admin/mnaccount');
    }
})

router.post('/admin/:id', async (req, res) => {
    const dl = await product.deleteById(req.params.id);
    res.redirect('/admin/mnproduct');
})

router.post('/updatesub/category=:idcat/sub=:idsub', urlencodedParser, async (req, res) => {
    console.log("b");
    const idcat = req.params.idcat;
    const idsub = req.params.idsub;
    const rs = await sub_cate.updateSub(idcat, idsub, req.body.subcate);
    res.redirect('/admin/mncategory=0');
})

router.post('/updatecate/category=:id', urlencodedParser, async (req, res) => {
    console.log("a");
    const id = req.params.id;
    console.log(id);
    const rs = await category.update(id, req.body.cate);
    res.redirect('/admin/mncategory=0');
})



router.post('/addcate', urlencodedParser, async (req, res) => {
    var cate = {
        CatID: 10,
        CatName: req.body.cate
    };
    const rs = await category.add(cate);
    res.redirect('/admin/mncategory=0');

})

router.post('/addsub=:id', urlencodedParser, async (req, res) => {
    const id = req.params.id;
    var sub = {
        use_CatID: id,
        sub_CatName: req.body.subcate
    };
    const rs = await sub_cate.addSub(sub);
    res.redirect('/admin/mncategory=0');
})
module.exports = router;