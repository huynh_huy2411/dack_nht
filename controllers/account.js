const express = require('express');
const router = express.Router();
const nodemailer = require('nodemailer');
const accountM = require('../models/account.M');
const bcrypt = require('bcryptjs');
const bodyParser = require('body-parser');
const { stringify } = require('querystring');
const request = require('request');
const fetch = require('node-fetch');

const bidders = require('../models/bidder_product/list_bidder');
const offer = require('../models/bidder_product/offer');

var t = 0;

router.use(express.json());
var jsonParser = bodyParser.json();

var urlencodedParser = bodyParser.urlencoded({ extended: false});
var err = false;

let transpoter = nodemailer.createTransport({
    service: 'gmail',
    secure: false,
    port: 25,
    auth: {
        user: 'bhanhmay31@gmail.com',
        pass: 'huykhung123'
    },
    tls: {
        rejectUnauthorized: false
    }
});

var userforget = "";


router.get('/login', async(req, res) => {
    res.render('home', {
        title: 'Login',
        Success : false,
        Login : true,
        Register: false,
        Errpassword: err,
    });
});

router.get('/register', (req, res) => {
    res.render('home', {
        title: 'Register',
        Success : false,
        Login : false,
        Register: true,
    });
});

router.get('/forget', urlencodedParser,async (req, res) => {
    // console.log(userforget);
    const getUser = await accountM.getByUsername(userforget);
    t = parseInt(parseInt(Date.now())/100000000);
    
    let HelperOptions = {
        from: '"San dau gia truc tuyen" <bhanhmay31@gmail.com>',
        to: 'cvtconfession@gmail.com',
        subject: "New password",
        text: `Your code: ${t}`,
    };
    transpoter.sendMail(HelperOptions, (error, info) => {
        if(error) {
            return console.log(error);
        }
        console.log("Message sent: " + info);
    });
    res.render('home', {
        Success : false,
        Login : false,
        Register: false,
        Forget: true,
    });
});

router.post('/login', urlencodedParser, async (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;
    userforget = username;
    const user = await accountM.getByUsername(username);
    if(user===null) {
        err = true;
        res.redirect('/login');
    }
   
    if( bcrypt.compareSync(password, user.user_Password)) {
        req.session.user = user.user_ID;
        req.value = true;
        res.redirect('/');
    }
    else {
       err = true;
       res.redirect('/login');
    }
});

router.post('/register', urlencodedParser, async (req, res) => {
    if(req.body.captcha === undefined || req.body.captcha === "" || req.body.captcha === null){
        return res.json({ success: false, msg: 'Please select captcha' });
    }

    // Secret key
     const secretKey = '6LdpvDEUAAAAAHszsgB_nnal29BIKDsxwAqEbZzU';

    //Verify URL
    const verifyURL = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`; 

    //Make request to Verify URL
    request(verifyURL, (err, response, body) => {
        body = JSON.parse(body);

        if(body.success != undefined && !body.success){
            console.log("abc");
            return res.json({ success: false, msg: 'Failed captcha verification' });
        }

       return ;
    });

    console.log("fa-odnoklassniki");

    const username = req.body.username;
    const password = req.body.password;

    var salt = bcrypt.genSaltSync(parseInt(parseInt(Date.now())/100000000000));
    
    var hash = bcrypt.hashSync(password, salt);

    const alluser = await accountM.getAll();

    var kt = true;

    //console.log(alluser[0].user_Email);

    for (const us of alluser) {
       
        if (us.user_Email == req.body.email) {
            kt = false; 
            break;
        }
    }
    // -------- reCaptcha v2 ------------


    // --------------------------------
    if (kt == true) {
        const user = {
            user_Account_Name: username,    
            user_Password: hash,
            user_Name: req.body.name,
            user_Email: req.body.email,
            user_Address: req.body.address,
            user_Decentralized: 2,
            user_Birthday: null,
        };
        const uId = await accountM.add(user);
        const id = await accountM.getByUsername(user.user_Account_Name);
        console.log(user.user_Password);
        req.session.user = id.user_ID;
        res.redirect('/');
    }
    else {
        res.render('home', {
            title: 'Register',
            Success : false,
            Login : false,  
            Register: true,
            Erremail: 'Email exists',
        });
    }
});

module.exports = router;