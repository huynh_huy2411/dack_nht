const express = require('express');
const router = express.Router();
const nodemailer = require('nodemailer');

const mCat = require('../models/category');
const mPro = require('../models/product');
const mSubCat = require('../models/sub_categories');
const accountM = require('../models/account.M');
const list_bidder = require('../models/bidder_product/list_bidder');
const offer = require('../models/bidder_product/offer');
const inter = require('../models/inter/interactive');
const up = require('../models/bidder_product/bidder_upgrade');

let transpoter = nodemailer.createTransport({
    service: 'gmail',
    secure: false,
    port: 25,
    pool: true,
    auth: {
        user: 'bhanhmay31@gmail.com',
        pass: 'huykhung123'
    },
    tls: {
        rejectUnauthorized: false
    }
    })

function parDatetime(date) {
    var now = "";
    now += date.getFullYear() + '-';
// --------------------------
    if ((date.getMonth() + 1) < 10) {
        now = now + '0' + (date.getMonth()+1) + '-';
    }
    else {
        now = now + (date.getMonth()+1) + '-';
    }
    // ---------------------------
    if (date.getDate() < 10) {
        now = now + '0' + date.getDate() + ' ';
    }
    else {
        now = now + date.getDate() + ' ';
    }
    // -------------------------
    if (date.getHours() < 10) {
        now = now + '0' + date.getHours() + ':';
    }
    else {
        now = now + date.getHours() + ':';
    }
    // ---------------------------
    if (date.getMinutes() < 10) {
        now = now + '0' + date.getMinutes() + ':';
    }
    else {
        now = now + date.getMinutes() + ':';
    }
    // ---------------
    if (date.getSeconds() < 10) {
        now = now + '0' + date.getSeconds();
    }
    else {
        now = now + date.getSeconds();
    }
    return now;
}

router.get('/logout', async (req,res,next) => {
    req.session.user = null;
    res.redirect('/');
});

var kt = 0;
router.use('/', async (req,res,next) => {
    if (kt == 0) {
    const pwin = await mPro.getProductExpWin();
    console.log(pwin);
    if (pwin != null){
    for (let awin of pwin) {
        const update = await list_bidder.setDone(awin.pro_ID);
        const bidder = await list_bidder.getWinnerByIDProduct(awin.pro_ID);
        const seller = await accountM.getByID(awin.use_sellerID);
        const addbidtosel = await inter.addBidderRateSeller(awin.pro_ID, awin.use_sellerID, bidder.use_userID);
        const addseltobid = await inter.addSellerRateBidder(awin.pro_ID, awin.use_sellerID, bidder.use_userID);

        if (bidder.user_Email != null) {           
        let HelperOptions = {
            from: '"San dau gia truc tuyen" <bhanhmay31@gmail.com>',
            to: `${bidder.user_Email}`,
            subject: `Đấu giá thành công`,
            text: `Sản phẩm ${awin.pro_Name} đã được đấu giá thành công!!`,
        };              
        transpoter.sendMail(HelperOptions, (error, info) => {
            if(error) {
                return console.log(error);
            }   
            //console.log("Message sent: " + info);
        });
        }

        if (seller.user_Email != null) {
        HelperOptions = {
            from: '"San dau gia truc tuyen" <bhanhmay31@gmail.com>',
            to: `${seller.user_Email}`,
            subject: `Đấu giá thành công`,
            text: `Sản phẩm ${awin.pro_Name} đã được đấu giá thành công!!`,
        };
        transpoter.sendMail(HelperOptions, (error, info) => {
            if(error) {
                return console.log(error);
            }   
            //console.log("Message sent: " + info);
        });
        }

        }
    }
    // kiem tra san pham het han ma khong co nguoi mua gui mail.      
    const pnull = await mPro.getProductExpNull();
    if (pnull != null) {
    for (let p of pnull) {
        const update = await mPro.setNotificated(p.pro_ID);
        const seller = await accountM.getByID(p.use_sellerID);
       if (seller != null) {
        let HelperOptions = {
            from: '"San dau gia truc tuyen" <bhanhmay31@gmail.com>',
            to: `${seller.user_Email}`,
            subject: `Sản phẩm hêt hạn`,
            text: `Sản phẩm ${p.pro_Name} của bạn đã hết hạn mà không có người nào mua!`,
        };                 
        transpoter.sendMail(HelperOptions, (error, info) => {
            if(error) {
                return console.log(error);
            }   
            //console.log("Message sent: " + info);
        }); 
        }
    }
    }
    }
    kt = 1;
    next();
})

router.get('/', async (req,res,next) => {
    const off = await offer.offers(1, 10, 23590000);

    var date = new Date();
    //console.log(date);
    
    const cats = await mCat.all();
    const sub_cats = await mSubCat.all();
    for (let cat of cats) {
        cat.isActive = false;
        const sub_cats = await mSubCat.allByCatID(cat.CatID);
        cat.sub_cats = sub_cats;
    }
    //cats[0].isActive = true;
   
    //console.log(req.session.user);
    if (req.session.user){
        session = req.session.user;
        profile = await accountM.getByID(session);

        switch (profile.user_Decentralized) {
            case 2:
                profile.bidder = true;
                break;
            case 3:
                profile.seller = true;
                break;
            case 4:
                profile.admin = true;
                break;
            default:
                profile.admin = null;
                
                profile.seller = null;
                
                profile.admin = null;
                break;
        };
    }
    else {
        session = null;
        profile = null;
    }
    //Lấy 5 sản phẩm gần kết thúc
    var date = Number(new Date());
    const ps = await mPro.all();
    for (let p of ps){
        p.time = Number(new Date(p.exp)) - date;
    }
    ps.sort(function(a, b) {
        return a.time - b.time;
    });
    var i = 0;
    //tìm vị trí i có time >0 (sản phẩm chưa kết thúc)
    for (let p of ps) {
        if (p.time <= 0) {
            i++;
        }
        else {
            break;
        }
    }

    const pro = ps.slice(i, i + 5);

    //Lấy 5 sản phẩm có nhiều lượt ra giá nhất
    var topTurn = ps;
    for (let p of topTurn){
        t = await list_bidder.getTotalTurnByProID(p.pro_ID);
        p.turn = parseInt(t);
    }
    
    //Sắp xếp theo số lượt ra giá từ cao đến thấp
    topTurn.sort(function(a, b) {
        return b.turn - a.turn;
    });

    //Lọc lại 5 sản phẩm cao nhất hoặc lấy sản phẩm có số lượt ra giá >0 nếu nhỏ hơn 5
    if (topTurn.length > 5){
        topTurn = topTurn.slice(0,5)
    }

    //Lấy 5 sản phẩm có giá cao nhất
    const topPrice = await mPro.getTopPriceProduct();

    res.render('home', {
        title: 'Product',
        cats: cats,
        top5: pro,
        topturn: topTurn,
        topprice: topPrice,
        profile: profile,
        Success: true,
        Login: false,
        Session: session,
    });
});

router.get('/logout', async (req,res,next) => {
    req.session.user = null;
    res.redirect('/');
});


// router.get(`/cat/:id/:subCatID/products/page=:page`, async(req,res) => {
router.get(`/cat/:id/:subCatID/products/page=:page&sortby=:sortby`, async(req,res) => {
    const id = parseInt(req.params.id);
    const subid = parseInt(req.params.subCatID);
    var page = parseInt(req.params.page) || 1;
    const sortby = req.params.sortby;
    
    const cats = await mCat.all();
    
    //Lấy danh sách sản phẩm theo trạng thái sortby
    if (sortby === 'alpha-asc'){
        rs = await mPro.allBySubCatIdPaging_1(id, subid, page);
    }
    if (sortby === 'alpha-desc'){
        rs = await mPro.allBySubCatIdPaging_2(id, subid, page);
    }
    if (sortby === 'price-asc'){
        rs = await mPro.allBySubCatIdPaging_3(id, subid, page);
    }
    if (sortby === 'price-desc'){
        rs = await mPro.allBySubCatIdPaging_4(id, subid, page);
    }
    if (sortby === 'created-desc'){
        const date = parDatetime(new Date());
        console.log(date);
        rs = await mPro.allBySubCatIdPaging_5(id, subid, page, date);
    }
    if (sortby === 'created-asc'){
        const date = parDatetime(new Date());
        console.log(date);
        rs = await mPro.allBySubCatIdPaging_6(id, subid, page, date);
    }
    
    //const rs = await mPro.allBySubCatIdPaging(id, subid, page);

    
    for (let cat of cats) {
        cat.isActive = false;
        const sub_cats = await mSubCat.allByCatID(cat.CatID);
        cat.sub_cats = sub_cats;
        if (cat.CatID == id) {
            cat.isActive = true;
            catName = cat.CatName;
        }
    }
    
    //Kiểm tra xem sản phẩm này có mới được up lên hay không
    //Nếu các sản phẩm mới được post lên trong vòng 1h thì sẽ được hiển thị New
    for (let p of rs.products){
        if(Number(new Date()) - Number(new Date(p.post_date)) <= 7200000){
            p.new = true;
        }
    }
    const links = `/cat/${id}/${subid}/products/page=`;
    const pages = [];
    for (let i = 0; i < rs.pageTotal; i++) {
        pages[i] = { value: i + 1, active: (i + 1) === page };
        pages[i].link = links;
        pages[i].sortby = sortby;
    }

    
    const navs = {};
    if (page > 1) {
        navs.prev = page -1;
    }
    if (page < rs.pageTotal) {
        navs.next = page + 1;
    }
    
    if (req.session.user){
        session = req.session.user;
        profile = await accountM.getByID(session);

        switch (profile.user_Decentralized) {
            case 2:
                profile.bidder = true;
                for (let p of rs.products) {
                    p.ktbidder = true;
                    p.Session = req.session.user;
                }
                break;
            case 3:
                profile.seller = true;
                break;
            case 4:
                profile.admin = true;
                break;
            default:
                profile.admin = null;
                
                profile.seller = null;
                
                profile.admin = null;
                break;
        };
    }
    else {
        session = null;
        profile = null;
    }

    if (sortby === 'alpha-asc'){
        alpha_asc = true;
        res.render('home', {
            title: 'Products',
            cats: cats,
            catName: catName,
            profile: profile,
            ps: rs.products,
            sortby: sortby,
            alpha_asc: alpha_asc,
            navs: navs,
            pages: pages,
            links: links,
            Session: session,
            Success: true,
            Login: false,
        });
    }
    if (sortby === 'alpha-desc'){
        alpha_desc = true;
        res.render('home', {
            title: 'Products',
            cats: cats,
            catName: catName,
            profile: profile,
            ps: rs.products,
            sortby: sortby,
            alpha_desc: alpha_desc,
            navs: navs,
            pages: pages,
            links: links,
            Session: session,
            Success: true,
            Login: false,
        });
    }
    if (sortby === 'price-asc'){
        price_asc = true;
        res.render('home', {
            title: 'Products',
            cats: cats,
            catName: catName,
            profile: profile,
            ps: rs.products,
            sortby: sortby,
            price_asc: price_asc,
            navs: navs,
            pages: pages,
            links: links,
            Session: session,
            Success: true,
            Login: false,
        });
    }
    if (sortby === 'price-desc'){
        price_desc = true;
        res.render('home', {
            title: 'Products',
            cats: cats,
            catName: catName,
            profile: profile,
            ps: rs.products,
            sortby: sortby,
            price_desc: price_desc,
            navs: navs,
            pages: pages,
            links: links,
            Session: session,
            Success: true,
            Login: false,
        });
    }
    if (sortby === 'created-desc'){
        created_desc = true;
        res.render('home', {
            title: 'Products',
            cats: cats,
            catName: catName,
            profile: profile,
            ps: rs.products,
            sortby: sortby,
            created_desc: created_desc,
            navs: navs,
            pages: pages,
            links: links,
            Session: session,
            Success: true,
            Login: false,
        });
    }
    if (sortby === 'created-asc'){
        created_asc = true;
        res.render('home', {
            title: 'Products',
            cats: cats,
            catName: catName,
            profile: profile,
            ps: rs.products,
            sortby: sortby,
            created_asc: created_asc,
            navs: navs,
            pages: pages,
            links: links,
            Session: session,
            Success: true,
            Login: false,
        });
    }
});


// router.get(`/cat/:id/products/page=:page`, async(req,res) => {
//     const id = parseInt(req.params.id);
//     const cats = await mCat.all();
//     const ps = await mPro.allByCatId(id);
//     var page = parseInt(req.params.page);

//     //var count = 5;
//     var currentpage = page;

//     for (let cat of cats) {
//         cat.isActive = false;
//         if (cat.CatID == id) {
//             cat.isActive = true;
//         }
//     }

//     var reps = [];
//     var repage = page;

//     if (page>=2) {
//     var repage = page - 1;
//     }
//     var nextpage = page;
    
//     if (nextpage < parseInt(ps.length/5+1)){
//         nextpage++;
//     }

//     var j = 0;

//     for (let i=(page-1)*5; i<page*5; i++) {
//         if(ps[i]) {
//         reps.push(ps[i]);
//         j++;
//         }
//     }

//     res.render('home', {
//         title: 'Products',
//         cats: cats,
//         ps: reps,
//         page: currentpage,
//         nextpage: nextpage,
//         prepage: repage,
//         id: id,
//         Success: true,
//         Login: false,
//     });
// });

// router.get(`/cat/:id/products/:ProID`, async(req,res) => {
//     const id = parseInt(req.params.id);
//     const idpro = parseInt(req.params.ProID);
//     const cats = await mCat.all();
//     const ps = await mPro.allByCatId(id);

//     for (let cat of cats) {
//         cat.isActive = false;
//         if (cat.CatID === id) {
//             cat.isActive == true;
//         }
//     }

//     for(let product of ps){
//         if(product.ProID === idpro){
//             ProInfo = product
//         }
//     }

//     res.render('home', {
//         title: 'Products',
//         cats: cats,
//         ProInfo: ProInfo,
//         Success: true,
//         Login: false,
//     });
// })











































// NGUYEN:: Thong tin chi tiet san pham
router.get(`/cat/:catID/:subCatID/products/:ProID`, async(req,res) => {
    const id = parseInt(req.params.catID);
    const idsub = parseInt(req.params.subCatID);
    const idpro = parseInt(req.params.ProID);

    const cats = await mCat.all();
    const subcats = await mSubCat.allByCatID(id);
    var ps = await mPro.allBySubCatId(idsub);
    const lb = await list_bidder.getListBidderByID(idpro);
    for (let lbs of lb) {
        lbs.price = lbs.price.toLocaleString();
        var i = 0;
        var str = "";
        while (lbs.user_Name.charAt(i) != ' ') {
            str += "*";
            i++;
        }
        i++;
        str += "*";
        while (i<lbs.user_Name.length) {
            str += lbs.user_Name.charAt(i);
            i++;
        }
        lbs.user_Name = str;
    }
    
    for (let cat of cats) {
        cat.isActive = false;
        if (cat.CatID === id) {
            cat.isActive = true;
        }
    }

    for (let subcat of subcats) {
        subcat.isActive = false;
        if (subcat.sub_CatID === idsub) {
            subcat.isActive = true;
        }
    }

    var i = 0;
    for(i;i<ps.length;i++){
        if(ps[i].pro_ID === idpro){
            ProInfo = ps[i];
            ps.splice(i, 1);
        }
    }

    //Tính toán thời điểm kết thúc (relative-time)
    const time = Number(Number(new Date(ProInfo.exp) - new Date()));
    if (time <= 0){
        ProInfo.time = 'Đã kết thúc';
    }
    else if (time > 86400000) { //Số mili giây của 1 ngày
        const day = parseInt(time / 86400000);
        ProInfo.time = `${day} ngày nữa`;
        console.log(ProInfo.time);
    } else if (time > 3600000) { //Số mili giây của 1 giờ
        const hour = parseInt(time / 3600000);
        ProInfo.time = `${hour} giờ nữa`;
        console.log(ProInfo.time);
    } else if (time > 60000) { //Số mili giây 1 phút
        const minute = parseInt(time / 60000);
        ProInfo.time = `${minute} phút nữa`;
        console.log(ProInfo.time);
    }
    console.log(ps.length);
    if (ps.length > 5){
        ps = ps.slice(0,5);
    }
    

    const seller = await accountM.getByID(ProInfo.use_sellerID);
    
    if (req.session.user){
        session = req.session.user;
        profile = await accountM.getByID(session);

        switch (profile.user_Decentralized) {
            case 2:
                profile.bidder = true;
                break;
            case 3:
                profile.seller = true;
                break;
            case 4:
                profile.admin = true;
                break;
            default:
                profile.admin = null;
                
                profile.seller = null;
                
                profile.admin = null;
                break;
        };
    }
    else {
        session = null;
        profile = null;
    }

    
    var proposal = (ProInfo.price + ProInfo.step);
    ProInfo.price = ProInfo.price;
    ProInfo.step = ProInfo.step;
    if (ProInfo.buy_now != null) {
    ProInfo.buy_now = ProInfo.buy_now.toLocaleString();
    } else {
        ProInfo.buy_now = null;
    }
    res.render('home', {
        title: 'Products',
        cats: cats,
        ps: ps,
        profile: profile,
        subcats: subcats,
        ProInfo: ProInfo,
        Proposal : proposal,
        ListBidder: lb,
        Seller: seller,
        Session: req.session.user,
        Success: true,
        Login: false,
    });
})



router.get('/search', async (req,res,next) => {
    
    const cats = await mCat.all();
    const sub_cats = await mSubCat.all();
    for (let cat of cats) {
        cat.isActive = false;
        const sub_cats = await mSubCat.allByCatID(cat.CatID);
        cat.sub_cats = sub_cats;
    }
    //cats[0].isActive = true;
   
    //console.log(req.session.user);

    if (req.session.user){
        session = req.session.user;
        profile = await accountM.getByID(session);

        switch (profile.user_Decentralized) {
            case 2:
                profile.bidder = true;
                break;
            case 3:
                profile.seller = true;
                break;
            case 4:
                profile.admin = true;
                break;
            default:
                profile.admin = null;
                
                profile.seller = null;
                
                profile.admin = null;
                break;
        };
    }
    else {
        session = null;
        profile = null;
    }

    //Lấy kết quả tìm kiếm
    const text = req.query.inputSearch;
    var ps = await mPro.fulltextSearch(text);
    console.log(req.query);
    const resultstring = `Kết quả tìm kiếm cho '${text}' (${ps.length} kết quả)`;
    
    //Kiểm tra kết quả trả về
    if (ps.length === 0){ //Không có kết quả
        res.render('home', {
            title: 'Product',
            cats: cats,
            resultstring: resultstring,
            searchresults: ps,
            profile: profile,
            Success: true,
            Login: false,
            Session: session,
        });
    }
    else {
        //Phân trang kết quả tìm kiếm
        const pageSize = 3;
        const pageTotal = Math.floor(ps.length / pageSize) + 1;
        const links = `/search?inputSearch=${text}&page=`;
        const page = parseInt(req.query.page) || 1;
        //console.log(page);
        const pages = [];
        for (let i = 0; i < pageTotal; i++) {
            pages[i] = { value: i + 1, active: (i + 1) === page };
            pages[i].link = links;
        }
    
    
        const navs = {};
        if (page > 1) {
            navs.prev = page -1;
        }
        if (page < pageTotal) {
            navs.next = page + 1;
        }
        //Lấy danh sách theo trang
        ps = ps.slice((page - 1) * pageSize, page * pageSize);

        //Kiểm tra xem sản phẩm này có mới được up lên hay không
        //Nếu các sản phẩm mới được post lên trong vòng 1h thì sẽ được hiển thị New
        for (let p of ps){
            if(Number(new Date()) - Number(new Date(p.post_date)) <= 7200000){
                p.new = true;
            }
        }

        res.render('home', {
            title: 'Product',
            cats: cats,
            resultstring: resultstring,
            searchresults: ps,
            catName: text,
            navs: navs,
            links: links,
            pages: pages,
            profile: profile,
            Success: true,
            Login: false,
            Session: session,
        });
    }
});

const bodyParser = require('body-parser');

var jsonParser = bodyParser.json();

var urlencodedParser = bodyParser.urlencoded({ extended: true });

router.post('/offer/id=:proid', urlencodedParser, async (req, res) => {
    const id = req.params.proid;
    const bidderid = req.session.user;
    const price = req.body.price;
    const rs = await offer.offers(id,bidderid,price);
    res.redirect('/profile/offerdoing');
})

router.post('/upgrade/id=:iduser', async (req, res) => {
    const id = req.params.iduser;
    const rs = await up.addUpgrade(id);
    res.redirect('/');
})

router.use('/', require('../controllers/mailOTP/mail'));
router.use('/', require('../controllers/account'));
router.use('/', require('../controllers/profile'))
router.use('/', require('../controllers/Admin/manager'));

module.exports = router;    
