const express = require('express');
const router = express.Router();
const nodemailer = require('nodemailer');

const account = require('../../models/account.M');
const products = require('../../models/product');

let transpoter = nodemailer.createTransport({
    service: 'gmail',
    secure: false,
    port: 25,
    auth: {
        user: 'bhanhmay31@gmail.com',
        pass: 'huykhung123'
    },
    tls: {
        rejectUnauthorized: false
    }
});

router.get('/refuse/ip=:idpro/ib=:idbidder', async (req,res)=> {
    const idpro = req.params.idpro;
    const idbidder = req.params.idbidder;
    console.log("refuse");
    const bidder = await account.getByID(idbidder);
    const product = await products.getProductByID(idpro);
    console.log(product);
    const email = bidder.user_Email;
    

    // let HelperOptions = {
    //     from: '"San dau gia truc tuyen" <bhanhmay31@gmail.com>',
    //     to: `${email}`,
    //     subject: `Refuse`,
    //     text: `Sản phẩm ${product.pro_Name} của bạn đã bị từ chối ra giá!`,
    // };
    // transpoter.sendMail(HelperOptions, (error, info) => {
    //     if(error) {
    //         return console.log(error);
    //     }   
    //     console.log("Message sent: " + info);
    // });
    res.redirect(`/cat/${product.CatID}/${product.sub_catID}/products/${product.pro_ID}`);
})  

module.exports = router;