const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');

const accountM = require('../models/account.M');
const pro = require('../models/product');
const noti = require('../models/inter/interactive');
const listlike = require('../models/bidder_product/list_bidder');
const seller = require('../models/sell_product/selling');

const bodyParser = require('body-parser');

var jsonParser = bodyParser.json();

var urlencodedParser = bodyParser.urlencoded({ extended: true });

var err = false;

router.use('/profile', function (req, res, next) {
    if (req.session.user == null) {
        res.redirect('/login');
    }
    else {
        next();
    }   
})

router.get('/profile', async (req, res) => {
    const id = req.session.user;
    const profile = await accountM.getByID(id);
    console.log(profile);
    res.render('home', {
        Profile: true,
        inf: profile,
        getInfo: true,
        updateInfo: false,
        editPassword: false,
        ll: false,
        rate: false,
        pro: false,
        doing: false,
        done: false
    })
});

router.get('/profile/update', async (req, res) => {
    const id = req.session.user;
    const profile = await accountM.getByID(id);
    console.log(profile);
    res.render('home', {
        Profile: true,
        inf: profile,
        getInfo: false,
        updateInfo: true,
        editPassword: false,
        ll: false,
        rate: false,
        pro: false,
        doing: false,
        done: false
    })
});

router.get('/profile/password', async (req, res) => {
    const id = req.session.user;
    const profile = await accountM.getByID(id);
    res.render('home', {
        Profile: true,
        inf: profile,
        getInfo: false,
        updateInfo: false,
        editPassword: true,
        oldpassword: true,
        rppassword: true,
        ll: false,
        rate: false,
        pro: false,
        doing: false,
        done: false
    })
});
router.post('/profile/update', urlencodedParser, async (req, res) => {

    console.log("form1");
    var user = {
    user_Name: req.body.name,
    user_Email: req.body.email,
    user_Birthday: req.body.birthday,
    };
    
    console.log(user);
    const id = req.session.user;
    const db = await accountM.update(id, user);
    
    res.redirect('/profile');
});

router.post('/profile/password', urlencodedParser, async (req, res) => {
    console.log("form2");
    const id = req.session.user;
    var oldpassword = req.body.oldpassword;
    var newpassword = req.body.newpassword;
    var rppassword = req.body.rppassword;
    const db = await accountM.getByID(id);
    const profile = await accountM.getByID(id);
    if( bcrypt.compareSync(oldpassword,db.user_Password)) {
        if (newpassword == rppassword) {
            var salt = bcrypt.genSaltSync(parseInt(parseInt(Date.now())/100000000000));
            var hash = bcrypt.hashSync(newpassword, salt);
            const edit = await accountM.editPassword(id, hash);
            res.redirect('/profile');
        }
        else {
            res.render('home', {
                Profile: true,
                inf: profile,
                getInfo: false,
                updateInfo: false,
                editPassword: true,
                oldpassword: true,
                rppassword: false,
                ll: false,
                rate: false,
                pro: false,
                doing: false,
                done: false
            })
        }
    } else {
        res.render('home', {
            Profile: true,
            inf: profile,
            getInfo: false,
            updateInfo: false,
            editPassword: true,
            oldpassword: false,
            rppassword: true,
            ll: false,
            rate: false,
            pro: false,
            doing: false,
            done: false
        })
    }
});

router.get('/profile/listlike', async(req, res) => {
    const list = await listlike.getListLike(req.session.user);
    res.render('home', {
        Profile: true,
        inf: profile,
        getInfo: false,
        updateInfo: false,
        editPassword: false,
        oldpassword: false,
        rppassword: false,
        ll: list,
        rate: false,
        pro: false,
        doing: false,
        done: false
    })
});

router.get('/profile/rate', async (req, res) => {

    const user = await accountM.getByID(req.session.user);

    if (user.user_Decentralized == 2) {
       
        var rate = await noti.getInteractiveBidder(req.session.user);
        let total = 0;
        let count = 0;
        for (let r of rate) {
            
        }
    }
    else if (user.user_Decentralized == 3) {
        var rate = await noti.getInteractiveSeller(req.session.user);
       
    }
  
    res.render('home', {
        Profile: true,
        inf: profile,
        getInfo: false,
        updateInfo: false,
        editPassword: false,
        oldpassword: false,
        rppassword: false,
        ll: false,
        rate: rate,
        pro: false,
        doing: false,
        done: false
    })
});

router.get('/profile/offerdoing', async (req, res) => {
    const user = await accountM.getByID(req.session.user);

    if (user.user_Decentralized == 2) {
        var doing = await listlike.getProductDoing(req.session.user);
        var done = await noti.getInfoProductBidderWinner(req.session.user);
        var bidder = true;
    }
    else if (user.user_Decentralized == 3) {
        var doing = await seller.getSelling(req.session.user);
        var done = await noti.getInfoProductSellerSucces(req.session.user);
        var bidder = false;
    }
    console.log(done);
    for (d of done)     {
        console.log(d);
        if (d.like == null && d.dislike == null && d.comment == null){
            d.form = true;
        }
        else {
            d.form = false;
        }
    }
   
    res.render('home', {
        Profile: true,
        inf: profile,
        getInfo: false,
        updateInfo: false,
        editPassword: false,
        oldpassword: false,
        rppassword: false,
        ll: false,
        rate: false,
        pro: true,
        doing: doing,
        done: done,
        bidder: bidder,
    })
})

router.get('/like/:idpro/:idbidder', async(req,res) => {
    const idpro = req.params.idpro;
    const idbidder = req.params.idbidder;
    const db = await listlike.addLikeList(idpro, idbidder);
    const product = await pro.getProductByID(idpro);
    res.redirect(`/cat/${product.CatID}/${product.sub_catID}/products/${product.pro_ID}`);
});

router.post('/profile/:pro/:seller/:bidder/:flag', urlencodedParser, async(req,res) => {
    const idpro = req.params.pro;
    const idseller = req.params.seller;
    const idbidder = req.params.bidder;
    
    if (req.params.flag == 1) { // bidder nhan xet seller
        if (req.body.like == 'like') {
        const rs = await noti.updateRate(idpro, idbidder, idseller, 1, 1, null, req.body.comment);
        } else {
        const rs = await noti.updateRate(idpro, idbidder, idseller, 1, null, 1, req.body.comment);
        }
        res.redirect('/profile/offerdoing');  
    }
    if (req.params.flag == 0) { // seller nhan xet bidder
        if (req.body.like == 'like') {
        const rs = await noti.updateRate(idpro, idbidder, idseller, 0, 1, null, req.body.comment);
        } else {
        const rs = await noti.updateRate(idpro, idbidder, idseller, 0, null, 1, req.body.comment);
        }
        res.redirect('/profile/offerdoing');  
    } 
                                                     
})


router.use('/', require('./seller/postProduct'));
module.exports = router;