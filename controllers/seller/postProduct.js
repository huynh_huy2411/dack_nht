const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
var multer  = require('multer');
const mkdirp = require('mkdirp');

const seller = require('../../models/account.M');
const cate = require('../../models/category');
const sub_cate = require('../../models/sub_categories');
const product = require('../../models/product');

var jsonParser = bodyParser.json();

var urlencodedParser = bodyParser.urlencoded({ extended: true });

var err = false;

function parDatetime(date) {
    var now = "";
    now += date.getFullYear() + '-';
// --------------------------
    if ((date.getMonth() + 1) < 10) {
        now = now + '0' + (date.getMonth()+1) + '-';
    }
    else {
        now = now + (date.getMonth()+1) + '-';
    }
    // ---------------------------
    if (date.getDate() < 10) {
        now = now + '0' + date.getDate() + ' ';
    }
    else {
        now = now + date.getDate() + ' ';
    }
    // -------------------------
    if (date.getHours() < 10) {
        now = now + '0' + date.getHours() + ':';
    }
    else {
        now = now + date.getHours() + ':';
    }
    // ---------------------------
    if (date.getMinutes() < 10) {
        now = now + '0' + date.getMinutes() + ':';
    }
    else {
        now = now + date.getMinutes() + ':';
    }
    // ---------------
    if (date.getSeconds() < 10) {
        now = now + '0' + date.getSeconds();
    }
    else {
        now = now + date.getSeconds();
    }
    return now;
}

router.get('/upload', async (req, res, next) => {
    
    if (req.session.user == null) {
        res.redirect('/login');
    }
    else {
        const decent = await seller.getByID(req.session.user);
       if(decent.user_Decentralized == 3) {
           next();
       } else {
           res.redirect('/');
       }
    }
});

router.get('/upload', async (req, res, next) => {
    const ct = await cate.all();
    const sub_ct = await sub_cate.all();

    res.render('home', {
        Upload: true,
        Success: true,
        cate: ct,
        subcate: sub_ct,
        Session: req.session.user,
    });
});

var catid = 0;
var subcatid = 0;
var idnewpro = 0;

router.post('/upload/cat', urlencodedParser, async (req, res, next) =>{
    catid = req.body.catid;
    subcatid = req.body.subcatid;
    const allpro = await product.all();
    idnewpro = allpro[allpro.length - 1].pro_ID + 1;
    console.log(idnewpro);
    res.redirect('/upload');
})



var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        const dir = `IMG/${catid}/${subcatid}/${idnewpro}`;

        mkdirp(dir, err => cb(err, dir))
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname)
    }
  })
   
var upload = multer({ storage: storage })

router.post('/upload', upload.array('avatar', 12), urlencodedParser, async(req, res) => {
   
    console.log(req.body.name + " " + req.body.price + " " + req.body.pricestep + " " + req.body.pricenow);
    console.log(req.body.datetime + " " + req.body.content);
    var date = new Date(req.body.datetime)
    console.log(parDatetime(date));
    // var date;
    // date = new Date();
    // console.log(date);
    // console.log(Number(date));
    // date1 = new Date('2019-12-24 17:16:41');
    // console.log(Number(date1));
    // console.log(Date(Number(date)));
    // console.log(parDatetime(date));
    
    // var d = new Date(Number(date) + parseInt(req.body.DAYS)*86400000);
    // console.log(parDatetime(d));
    res.redirect(`/cat/1/1/products/1`);
    //res.redirect(`/cat/${catid}/${subcatid}/products/${idnewpro}`);
});

module.exports = router;