const express = require('express'),
    app = express(),
    port = 5000,
    exphbs = require('express-handlebars');

const hbs = exphbs.create({
    defaultLayout: 'start',
    extname: 'hbs',
});

const session = require('express-session');

app.use(session({
    secret: 'qweasdzxc',
    resave: false,
    saveUninitialized: true,
}));
    
app.engine('hbs', hbs.engine);

app.set('view engine', 'hbs');

app.use(express.static(__dirname + ''));

app.use('/', require('./controllers/category'));
app.use('/login', require('./controllers/account'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));  