const db = require('../utils/db2');
const tbName = 'account';

module.exports = {
    add: async (user) => {
        const id = await db.add(tbName, user);
        return id;
    },
    getByUsername: async (username) => {
        var sql = `SELECT * FROM ${tbName} WHERE user_Account_Name = '${username}'`;
       
        const rs = await db.load(sql);
       
        if(rs.length > 0) {
            return rs[0];
        }
        return null;
    },
    getAll: async () => {
        var sql = `SELECT * FROM ${tbName}`;
        const rs = await db.load(sql);
        console.log(rs);
        return rs;
    },
    getByID: async (id) => {
        var sql = `SELECT * FROM ${tbName} WHERE user_ID = ${id}`;
        const rs = await db.load(sql);
        return rs[0];
    },
    update: async (id, user) => {
        var set = `user_Name = '${user.user_Name}', user_Email = '${user.user_Email}', user_Birthday = '${user.user_Birthday}'`;
        var where = `user_ID = ${id}`;
        const rs = await db.update(tbName,set,where);
        return rs;
    },
    editPassword: async (id,user_Password) => {
        var set =`user_Password = '${user_Password}'`;
        var where = `user_ID = ${id}`;
        const rs = await db.update(tbName,set,where);
        return rs;
    },
    setUpgradeBidder: async (id) => { // upgrade lên
        var set = `user_Decentralized = 3`;
        var where = `user_ID = ${id}`;
        const rs = await db.update(tbName,set,where);
        return rs;
    },
    setDropSeller: async (id) => {
        var set = `user_Decentralized = 2`;
        var where = `user_ID = ${id}`;
        const rs = await db.update(tbName,set,where);
        return rs;
    }
}