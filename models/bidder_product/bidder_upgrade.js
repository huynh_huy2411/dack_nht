const db = require('../../utils/db2');
const tbUp = 'upgrade';

module.exports = {
    getNewUpgrade : async (id) => { // for mailsystem Admin
        const sql = `SELECT * FROM ${tbUp} WHERE ${tbUp}.use_userID = ${id} AND ${tbUp}.up = 0`;
        const rs = await db.load(sql);
        return rs;
    },
    setUpgrade : async (id) => {
        const set = `up = 1`;
        const where = `use_userID = ${id}`;
        const rs = await db.update(tbUp,set,where);
        return rs;
    },
    addUpgrade : async (id) => {
       const up = {
           use_userID : id,
           up : 0,
           new : 0
    };
    const rs = await db.add(tbUp,up);
    return rs;
}
}