const db = require('../../utils/db2');
const tbBidder = 'account';
const tbProduct = 'products';
const tbListBidder = 'list_bidder';
const tbListLike = 'list_like';

module.exports = {
    getListBidderByID: async (ids) => { // Lay danh sach Bidder theo ID product
        const sql = `SELECT * FROM ${tbBidder}, ${tbListBidder} WHERE ${tbBidder}.user_ID = ${tbListBidder}.use_userID AND ${tbListBidder}.use_proID = ${ids} AND ${tbListBidder}.refuse = 1 ORDER BY ${tbListBidder}.price DESC`;
        console.log("abc" + sql)    ;
        console.log("av");
        const rs = await db.load(sql);
        return rs;
    },
    getListLike: async (id) => { // Lay danh sach san pham yeu thich theo ID bidder
        var sql =`SELECT * FROM ${tbListLike}, ${tbProduct} WHERE ${tbListLike}.use_proID = ${tbProduct}.pro_ID AND ${tbListLike}.use_userID = ${id}`;
        const rs = await db.load(sql);
        return rs;
    },
    getProductDoing: async(id) => { // lay danh sach cac san pham dang dau gia cua bidder
        var sql = `SELECT * FROM ${tbListBidder}, ${tbProduct} WHERE ${tbListBidder}.use_proID = ${tbProduct}.pro_ID AND ${tbListBidder}.use_userID = ${id} AND ${tbListBidder}.flag = 1 AND ${tbListBidder}.refuse != -1`;
        const rs = await db.load(sql);
        return rs;
    },
    getWinnerByIDProduct: async(id) => { // Llay nguoi thang dau giatheo id san pham tra ve 1 phan tu trong list bidder
        var sql = `SELECT * FROM ${tbListBidder} WHERE use_proID = ${id} AND refuse = 1 AND flag = -1 ORDER BY price DESC`;
        const rs = await db.load(sql);
        const winner = rs[0];
        return winner;
    },
    checkRefuse: async (idPro, idUser) => { // kiem tra id cua bidder co bi tu choi ra gia cho san pham nay khong
        var sql = `SELECT * FROM ${tbListBidder} WHERE use_userID = ${idUser} AND use_proID = ${idPro} AND refuse = -1`;
        const rs = await db.load(sql);
        return rs;
    },
    addLikeList: async (idPro, idUser) => { // thêm vào danh sach yeu thich
        var like = {
            use_proID: idPro,
            use_userID: idUser
        };
        const rs = await db.add(tbListLike, like);
        return rs;
    },
    setDone: async (idPro) => { // set là đã hết tgian đấu giá
        var set = `flag = -1`;
        var where = `use_proID = ${idPro}`;
        const rs = await db.update(tbListBidder, set, where);
        return rs;
        },
        
    getTotalTurnByProID: async(id) => { //Lấy tổng số lượt ra giá của một sản phẩm
        var sql = `SELECT count(*) AS Total FROM ${tbListBidder} WHERE use_proID = ${id} AND refuse = 1`;
        const rs = await db.load(sql);
        //console.log(rs[0].Total);
        return (parseInt(rs[0].Total));
    }
};