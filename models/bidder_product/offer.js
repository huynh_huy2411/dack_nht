// Ra giá
const db = require('../../utils/db2');
const lb = require('./list_bidder');
const prod = require('../product');
const user = require('../account.M');
const tbName = 'list_bidder';

const nodemailer = require('nodemailer');

let transpoter = nodemailer.createTransport({
    service: 'gmail',
    secure: false,
    port: 25,
    pool: true,
    auth: {
        user: 'bhanhmay31@gmail.com',
        pass: 'huykhung123'
    },
    tls: {
        rejectUnauthorized: false
    }
});

function parDatetime(date) {
    var now = "";
    now += date.getFullYear() + '-';
// --------------------------
    if ((date.getMonth() + 1) < 10) {
        now = now + '0' + (date.getMonth()+1) + '-';
    }
    else {
        now = now + (date.getMonth()+1) + '-';
    }
    // ---------------------------
    if (date.getDate() < 10) {
        now = now + '0' + date.getDate() + ' ';
    }
    else {
        now = now + date.getDate() + ' ';
    }
    // -------------------------
    if (date.getHours() < 10) {
        now = now + '0' + date.getHours() + ':';
    }
    else {
        now = now + date.getHours() + ':';
    }
    // ---------------------------
    if (date.getMinutes() < 10) {
        now = now + '0' + date.getMinutes() + ':';
    }
    else {
        now = now + date.getMinutes() + ':';
    }
    // ---------------
    if (date.getSeconds() < 10) {
        now = now + '0' + date.getSeconds();
    }
    else {
        now = now + date.getSeconds();
    }
    return now;
}

module.exports = {
    offers: async (idPro, idUser, price) => {
        // Kiem tra co bi tu choi ra san pham hay khong
        const rf = await lb.checkRefuse(idPro, idUser);
        console.log(rf);

        if (rf.length != 0) {
            return {
                kt: false,
                message: "Bạn bị từ chối ra giá cho sản phẩm này",
            };
        }

        const rs = await lb.getListBidderByID(idPro);
        const pro = await prod.getProductByID(idPro);

        if (pro.accept == 0) {
            const acc = await user.getByID(idUser);
            if (acc[0].user_Point < 80.0 || acc[0].user_Point == null) {
                return {
                    kt: false,
                    message: "Rate phải đạt 8.0 trở lên mới được đấu giá sản phẩm!!",
                };
            }
        }
        // Kiem tra ve gia ra phai lon hon va la boi cua buoc gia
        if (rs.length != 0) {

        const priceHighest = rs[0].price;
        if (price <= priceHighest){
            return {
                kt: false,
                message: "Giá bạn ra thấp hơn giá hiện tại!!"
            };
        }
        else {
            var step = (price - pro.price) % pro.step;
            console.log(step);
            if (parseInt(step) == 0) {
                kt = true;
                var message = "Giá hợp lí!!";
            }
            else {
                return {
                    kt: false,
                    message: "Giá bạn ra phải là bội của bước giá!!"
                }
            }
        }
        }

        var date = new Date();
        // Them vao danh sach bidder
        const addABidder = {
            use_proID : idPro,
            use_userID : idUser,
            price : price,
            datetime : parDatetime(date),
            flag : 1,
            refuse : 1
        };

        if (rs.length != 0) {
        const holdBidder = rs[0].use_userID; // id người giữ giá 
        
        const hold = await user.getByID(holdBidder);
        
        let HelperOptions = {
            from: '"San dau gia truc tuyen" <bhanhmay31@gmail.com>',
            to: `${hold.user_Email}`,
            subject: `Bạn bị mất quyền giữ giá sản phẩm`,
            text: `Sản phẩm ${pro.pro_Name} mà bạn đang giữ giá đã có người trả giá cao hơn!!`,
        };

        transpoter.sendMail(HelperOptions, (error, info) => {
            if(error) {
                return console.log(error);
            }   
            //console.log("Message sent: " + info);
        });
        }

        const mm = await db.add(tbName, addABidder);

        return {
            kt: true,
            message: "Ra giá thành công!!"
        }
    }
}