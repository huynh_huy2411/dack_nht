const db = require('../../utils/db2');
const tbListBidder = require('list_bidder');

module.exports = {
    refuse : async (idPro, idBidder) => { // từ chối bidder
        const set = `refuse = -1`;
        const where = `use_proID = ${idPro} AND use_userID = ${idBidder}`;
        var sql = await db.update(tbListBidder,set,where);
        return sql;
    }
}