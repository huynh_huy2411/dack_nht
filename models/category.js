const db = require('../utils/db2');
const tbName = 'categories';

module.exports = {
    all: async () => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        return rows;
    },

    update: async (id, name) => {
        const set = `CatName = '${name}'`;
        const where = `CatID = ${id}`;
        const rs = await db.update(tbName,set,where);
        return rs;
    },

    add: async (category) => {
        const rs = await db.add(tbName,category);
        return rs;
    }
}