const db = require('../../utils/db2');
const tbNoti = ('notification');
const tbAccount = ('account');
const tbPro = ('products');

module.exports = {
    getInteractiveBidder: async (idUser) => { // lấy ra những tương tác tới bidder
        const sql = `SELECT * FROM ${tbNoti}, ${tbAccount} WHERE ${tbAccount}.user_ID = ${tbNoti}.use_sellerID AND ${tbNoti}.use_bidderID = ${idUser} AND ${tbNoti}.flag = 0 AND (${tbNoti}.like IS NOT null OR ${tbNoti}.dislike IS NOT null OR ${tbNoti}.comment IS NOT null)`;
        const rs = await db.load(sql);
        return rs;
    },
    getInfoProductBidderWinner: async (idUser) => { // lấy ra danh sach san pham thang dau gia                                                    
        const sql = `SELECT * FROM ${tbNoti}, ${tbAccount}, ${tbPro} WHERE ${tbPro}.pro_ID = ${tbNoti}.use_proID AND ${tbAccount}.user_ID = ${tbNoti}.use_sellerID AND ${tbNoti}.use_bidderID = ${idUser} AND ${tbNoti}.flag = 1 AND ${tbNoti}.win = 1`;
        const rs = await db.load(sql);
        return rs;
    },
    getInteractiveSeller: async(idUser) => { // lấy ra những tương tác tới seller
        const sql = `SELECT * FROM ${tbNoti}, ${tbAccount} WHERE ${tbAccount}.user_ID = ${tbNoti}.use_bidderID AND ${tbNoti}.use_sellerID = ${idUser} AND ${tbNoti}.flag = 1 AND (${tbNoti}.like IS NOT null OR ${tbNoti}.dislike IS NOT null OR ${tbNoti}.comment IS NOT null)`;
        const rs = await db.load(sql);
        console.log(sql);   
        return rs;
    },
    getInfoProductSellerSucces: async (idUser) => { // lấy ra danh sach san pham thang dau gia                                                    
        const sql = `SELECT * FROM ${tbNoti}, ${tbAccount}, ${tbPro} WHERE ${tbPro}.pro_ID = ${tbNoti}.use_proID AND ${tbAccount}.user_ID = ${tbNoti}.use_bidderID AND ${tbNoti}.use_sellerID = ${idUser} AND ${tbNoti}.flag = 0 AND ${tbNoti}.win = 1`;
        const rs = await db.load(sql);
        return rs;
    },
    addBidderRateSeller: async(idPro, idSeller, idBidder,) => { // thêm một đánh giá của bidder cho selller khi sản phẩm đáu giá thành công
        const nt = {
            use_proID: idPro,
            use_sellerID: idSeller,
            use_bidderID: idBidder,
            like: null,
            flag: 1,
            dislike: null,
            comment: null,
            win: 1
        };
        const rs = await db.add(tbNoti, nt);
        return rs;
    },
    addSellerRateBidder: async (idPro, idSeller, idBidder) => { // thêm một đanh giá của seller cho bidder khi sản phẩm đáu giá thành công
        const nt = {
            use_proID: idPro,
            use_sellerID: idSeller,
            use_bidderID: idBidder,
            like: null,
            flag: 0,
            dislike: null,
            comment: null,
            win: 1
        };
        const rs = await db.add(tbNoti, nt);
        return rs;
    },
    addSellerCancleBidder: async (idPro, idSeller, idBidder) => { // thêm một đanh giá của seller cho bidder khi cancle 
        const nt = {
            use_proID: idPro,
            use_sellerID: idSeller,
            use_bidderID: idBidder,
            like: null,
            flag: 0,
            dislike: null,
            comment: null,
            win: 0
        };
        const rs = await db.add(tbNoti, nt);
        return rs;
    },
    updateRate: async (idPro, idBidder, idSeller, flag, like, dislike, comment) => {
        const set = `${tbNoti}.like = ${like}, ${tbNoti}.dislike = ${dislike}, ${tbNoti}.comment = '${comment}'`;
        const where = `use_proID = ${idPro} AND use_bidderID = ${idBidder} AND use_sellerID = ${idSeller} AND flag = ${flag}`;
        const rs = await db.update(tbNoti,set,where);
        return rs;
    },
    getPointRateSeller: async (use_sellerID, idUser)=> {
        if (use_sellerID == 'use_sellerID'){
            var flag = 1;
        } else {
            var flag = 0;
        }
        const sql = `SELECT * FROM ${tbAccount}, ${tbNoti} WHERE ${tbAccount}.user_ID = ${tbNoti}.${use_sellerID} AND ${tbNoti}.${use_sellerID} = ${idUser} AND ${tbNoti}.flag = ${flag}`;
        const rs = await db.load(sql);
        var count = 0;
        var total = 0;
        for (let rate of rs) {
            count++;
            if (rate.like == 1) {
                total += 1;
            } else {
                total -= 1;
            }
        }

        const set = `user_Point = ${total}*100.0/${count}`;
        const where = `user_ID = ${idUser}`;
        const update = await db.update(tbAccount,set,where);
        return total*100/count;
    }
}