const db = require('../utils/db2');
const tbName = 'notification';

module.exports = {
    getNotificationByBidderId: async (id) => {
        var sql = `SELECT * FROM ${tbName} WHERE use_bidderID=${id}`;
        const rs = await db.load(sql);
        console.log(rs);
        return rs;
    },
    getNotificationBySellerId: async (id) => {
        var sql = `SELECT * FROM ${tbName} WHERE use_sellerID=${id}`;
        const rs = await db.load(sql);
        console.log(rs);
        return rs;
}
}