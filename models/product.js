const db = require('../utils/db2');
const tbName = 'products';
const tbListBidder = 'list_bidder';
const pageSize = 3;

module.exports = {
    all: async () => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        return rows;
    },
    allByCatId: async (id) => {
        const sql = `SELECT * FROM ${tbName} WHERE CatID = ${id}`;
        const rows = await db.load(sql);
        return rows;
    },
    allBySubCatId: async (id) => {
        const sql = `SELECT * FROM ${tbName} WHERE sub_catID = ${id}`;
        const rows = await db.load(sql);
        return rows;
    },
    allBySubCatIdPaging: async (id, subid, page) => {
        let sql = `SELECT count(*) AS total FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid}`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.floor(totalP / pageSize) + 1;
        const offset = (page - 1) * pageSize;
        sql = `SELECT * FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid} LIMIT ${pageSize} OFFSET ${offset}`;
        const rows = await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows
        };
    },
    getProductByID: async (id) => {
        let sql = `SELECT * FROM ${tbName} WHERE pro_ID = ${id}`
        const rs = await db.load(sql);
        return rs[0];
        },
        
    allBySubCatIdPaging_1: async (id, subid, page) => { //Tên từ A -> Z
        let sql = `SELECT count(*) AS total FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid}`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.floor(totalP / pageSize) + 1;
        const offset = (page - 1) * pageSize;
        sql = `SELECT * FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid} ORDER BY pro_Name ASC LIMIT ${pageSize} OFFSET ${offset}`;
        const rows = await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows
        };
    },
    allBySubCatIdPaging_2: async (id, subid, page) => { //Tên từ Z -> A
        let sql = `SELECT count(*) AS total FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid}`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.floor(totalP / pageSize) + 1;
        const offset = (page - 1) * pageSize;
        sql = `SELECT * FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid} ORDER BY pro_Name DESC LIMIT ${pageSize} OFFSET ${offset}`;
        const rows = await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows
        };
    },
    allBySubCatIdPaging_3: async (id, subid, page) => { //Giá tăng dần
        let sql = `SELECT count(*) AS total FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid}`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.floor(totalP / pageSize) + 1;
        const offset = (page - 1) * pageSize;
        sql = `SELECT * FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid} ORDER BY price ASC LIMIT ${pageSize} OFFSET ${offset}`;
        const rows = await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows
        };
    },
    allBySubCatIdPaging_4: async (id, subid, page) => { //Giá giảm dần
        let sql = `SELECT count(*) AS total FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid}`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.floor(totalP / pageSize) + 1;
        const offset = (page - 1) * pageSize;
        sql = `SELECT * FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid} ORDER BY price DESC LIMIT ${pageSize} OFFSET ${offset}`;
        const rows = await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows
        };
    },
    allBySubCatIdPaging_5: async (id, subid, page, time) => { //Thời gian kết thúc giảm
        let sql = `SELECT count(*) AS total FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid} AND exp > '${time}'`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.floor(totalP / pageSize) + 1;
        const offset = (page - 1) * pageSize;
        sql = `SELECT * FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid} AND exp > '${time}' ORDER BY exp ASC LIMIT ${pageSize} OFFSET ${offset}`;
        const rows = await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows
        };
    },
    allBySubCatIdPaging_6: async (id, subid, page, time) => { //Thời gian kết thúc tăng
        let sql = `SELECT count(*) AS total FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid} AND exp > '${time}'`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.floor(totalP / pageSize) + 1;
        const offset = (page - 1) * pageSize;
        sql = `SELECT * FROM ${tbName} WHERE CatID=${id} AND sub_catID=${subid} AND exp > '${time}' ORDER BY exp DESC LIMIT ${pageSize} OFFSET ${offset}`;
        const rows = await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows
        };
    },
    addProduct: async (product) => {
        let sql = await db.add(tbName, product);
        return sql;
    },
    fulltextSearch: async (text) => {
        let sql = `SELECT * FROM ${tbName} WHERE MATCH(pro_Name) AGAINST('${text}' IN NATURAL LANGUAGE MODE)`;
        const rows = await db.load(sql);
        return rows;
    },
    getProductExpWin: async () => { // lấy ra những san phẩm hết hạn + nguoi thang                
        var date = new Date();
        const sql = `SELECT DISTINCT pro_Name ,pro_ID, use_sellerID, exp FROM ${tbName}, ${tbListBidder} WHERE ${tbName}.pro_ID = ${tbListBidder}.use_proID AND ${tbListBidder}.refuse = 1 AND ${tbListBidder}.flag = 1`;        
        var rs = await db.load(sql);
        console.log(rs.length);
        let expArray = [];
        for (const pro of rs) {
            let dl = new Date(pro.exp);
            if ((Number(dl) - Number(date)) <= 0) {
                expArray.push(pro);
        }
    }
    return expArray;    
    },
    getProductExpNull: async () => { // lấy ra những san phẩm hết hạn + khong co nguoi mua         
        var date = new Date();
        const sql = `SELECT DISTINCT pro_ID FROM ${tbName}, ${tbListBidder} WHERE ${tbName}.notification IS null AND ${tbName}.pro_ID = ${tbListBidder}.use_proID AND ${tbListBidder}.refuse = 1`;        
        var rs = await db.load(sql);
     
        let expArray = [];
        for (const pro of rs) {
                old = pro.pro_ID;
                expArray.push(old);
        }   
    // }
    
    if (rs.length != 0) {
        const sqln = `SELECT pro_ID, pro_Name, use_sellerID FROM ${tbName} WHERE pro_ID NOT IN (${expArray})`;
        console.log(sqln);  
        var rsn = await db.load(sqln);
        return rsn;
        }
    return null;
    },
    setNotificated: async (idPro) => {
        var set = `notification = 1`;
        var where = `pro_ID = ${idPro}`;
        const rs = await db.update(tbName,set,where);
        return rs;
    },
    deleteById: async (id) => {
        var where = `pro_ID = ${id}`;
        const rs = await db.delete(tbName,where);
    },

    getTopPriceProduct: async () => {
        let sql = `SELECT * FROM ${tbName} ORDER BY price DESC LIMIT 5 OFFSET 0`;
        const rs = await db.load(sql);
        return rs;
    }
 }                     