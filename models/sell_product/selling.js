const db = require('../../utils/db2');
const tbBidder = 'account';
const tbProduct = 'products';
const tbListBidder = 'list_bidder';
const tbListLike = 'list_like';

function parDatetime(date) {
    var now = "";
    now += date.getFullYear() + '-';
// --------------------------
    if ((date.getMonth() + 1) < 10) {
        now = now + '0' + (date.getMonth()+1) + '-';
    }
    else {
        now = now + (date.getMonth()+1) + '-';
    }
    // ---------------------------
    if (date.getDate() < 10) {
        now = now + '0' + date.getDate() + ' ';
    }
    else {
        now = now + date.getDate() + ' ';
    }
    // -------------------------
    if (date.getHours() < 10) {
        now = now + '0' + date.getHours() + ':';
    }
    else {
        now = now + date.getHours() + ':';
    }
    // ---------------------------
    if (date.getMinutes() < 10) {
        now = now + '0' + date.getMinutes() + ':';
    }
    else {
        now = now + date.getMinutes() + ':';
    }
    // ---------------
    if (date.getSeconds() < 10) {
        now = now + '0' + date.getSeconds();
    }
    else {
        now = now + date.getSeconds();
    }
    return now;
}

module.exports = {
    getSelling: async (id) => {
        var date = new Date();
        var sql = `SELECT * FROM ${tbProduct} WHERE ${tbProduct}.use_sellerID = ${id}`;
        const rs = await db.load(sql);
        var arr = [];
        for (let r of rs) {
            var exp = new Date(r.exp);
            if (Number(exp) - Number(date) > 0) {
                arr.push(r);
            }
    }
    return arr;
    }
}