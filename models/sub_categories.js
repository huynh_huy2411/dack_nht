const db = require('../utils/db2');
const tbName = 'sub_categories';
const tbCate = 'categories';
module.exports = {
    all: async () => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        return rows;
    },
    
    allByCatID: async (id) => {
        const sql = `SELECT * FROM ${tbName} WHERE use_CatID = ${id}`
        const rows = await db.load(sql);
        //console.log(rows);
        return rows;
    },

    updateSub: async (idCat, idSub, name) => {
        const set = `sub_CatName = '${name}'`;
        const where = `use_CatID = ${idCat} AND sub_CatID = ${idSub}`;
        const rs = await db.update(tbName,set,where);
        return rs;
    },

    addSub: async (sub) => {
        const rs = await db.add(tbName, sub);
        return rs;
    }
   
}