const mysql = require('mysql');

function createConnection() {
    return mysql.createConnection({
        host: 'localhost',
        port: '8889',
        user: 'root',
        password: 'root',
        database: 'news'
    });
}

exports.load = sql => {
    return new Promise((resole, reject) => {
        const con = createConnection();
        con.connect(err => {
            if(err) {
                console.log("OK");
                reject(err);
            }
        });
        con.query(sql, (error, results, fields) => {
            if (error) {           
                reject(error);
            }
            resole(results);
        });
        con.end(); 
    });
};

exports.add = (tbName, user) => {
    return new Promise((resole, reject) => {
        const con = createConnection();
        con.connect(err => {
            if(err) {
                console.log("OK");
                reject(err);
            }
        });
        var sql = `INSERT INTO ${tbName} SET ?`;
        con.query(sql, user, (error, results, fields) => {
            if (error) {           
                reject(error);
            }
            resole(results);
        });
        con.end(); 
    });
};

exports.update = (tbName, set, where) => {
    return new Promise((resole, reject) => {
        const con = createConnection();
        con.connect(err => {
            if(err) {
                console.log("OK");
                reject(err);
            }
        });
        var sql =`UPDATE ${tbName} SET ${set} WHERE ${where}`;
        con.query(sql, (error, results, fields) => {
            if (error) {           
                reject(error);
            }
            resole(results);
            });
            con.end(); 
    });
}

exports.delete = (tbName, where) => {
    return new Promise((resole, reject) => {
        const con = createConnection();
        con.connect(err => {
            if(err) {
                console.log("OK");
                reject(err);
            }
        });
        var sql =`DELETE FROM ${tbName} WHERE ${where}`;
        con.query(sql, (error, results, fields) => {
            if (error) {           
                reject(error);
            }
            resole(results);
            });
            con.end(); 
    });
}